﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace XSLTTransformation
{
    class Program
    {
        static void Main(string[] args)
        {
            string man_ID = "Milton";

            XsltArgumentList argsList = new XsltArgumentList();
            argsList.AddParam("ReqYear", "", 1982);

            XslCompiledTransform transform = new XslCompiledTransform(true);
            transform.Load("cdCatalog.xslt");

            using (StreamWriter sw = new StreamWriter("output.html"))
            {
                transform.Transform("Catalog.xml", argsList, sw);
            }

        }
    }
}
